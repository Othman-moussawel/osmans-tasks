const mongoose = require('mongoose');

const Product = require('../models/product');

exports.products_get_template =(req,res,next)=>{
 // app.get('/download', function(req, res){
    const file = '../buyparts24-template.xlsx';
    res.download(file); // Set disposition and send it.
// });
}
exports.products_create_product = (req, res, next) => {
  //making a product of type Product to store data in it, the objectid() generates a unique id
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    productFile: req.file.path
    //also need to store in the db the path of the image that is located on my pc in order to retreive them
    //was reading from body
  });
  //saving the product into the db
  product
    .save()
    .then(result => {
      //here i'm logging the successful result and returning the product
      console.log(result);
      res.status(201).json({
        message: "product uploaded Successfully",

      });
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    });


}
